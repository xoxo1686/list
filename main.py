import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = 0#driver.find_element_by_id('th').text
    ah = 0#driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-27 09:35:02.863651
#2018-02-27 09:52:01.332840
#2018-02-27 10:08:01.536826
#2018-02-27 10:36:01.310483
#2018-02-27 11:01:02.051705
#2018-02-27 11:14:02.075699
#2018-02-27 11:27:01.419497
#2018-02-27 11:45:01.586615
#2018-02-27 12:04:01.699026
#2018-02-27 12:26:01.795371
#2018-02-27 12:49:01.988091
#2018-02-27 13:17:01.595412
#2018-02-27 13:37:01.746019
#2018-02-27 14:07:01.275194
#2018-02-27 14:21:01.523611
#2018-02-27 14:44:01.248643
#2018-02-27 15:06:01.387717
#2018-02-27 15:22:02.187130
#2018-02-27 15:48:01.701607
#2018-02-27 16:12:01.418138
#2018-02-27 16:36:02.959538
#2018-02-27 16:57:01.998466
#2018-02-27 17:48:01.435057
#2018-02-27 18:47:01.379147
#2018-02-27 19:32:02.101037
#2018-02-27 20:36:01.773882